import React, { Component } from "react";
import { connect } from "react-redux";

class Student extends Component {
  constructor(props) {
    super(props);
    this.state = {
      student: this.props.StuList,
    };
  }
  renderListStu = () => {
    let { studentList, keyWord } = this.props.StuList;
    console.log(keyWord);
    console.log(studentList);
    studentList = studentList.filter((stu) => {
      return stu.name.toLowerCase().indexOf(keyWord.toLowerCase()) !== -1;
    });
    return studentList.map((stu) => {
      return (
        <tr key={stu.id}>
          <th scope="row">{stu.id}</th>
          <td>{stu.name}</td>
          <td>
            <img width={100} height={80} src={stu.image} alt="" />
          </td>
          <td>{stu.price}</td>
          <td>{stu.description}</td>
          <td>{stu.type}</td>
          <td>
            <i
              className="fa-regular fa-pen-to-square"
              data-bs-toggle="modal"
              data-bs-target="#staticBackdrop"
              onClick={() => {
                this.props.onEditStd(stu);
              }}
            ></i>
          </td>
          <td>
            <i
              className="fa-regular fa-trash-can"
              onClick={() => {
                this.props.onDeleteStd(stu.id);
              }}
            ></i>
          </td>
        </tr>
      );
    });
  };
  static getDerivedStateFromProps(nextProps, currentState) {
    console.log(currentState);

    return currentState;
  }
  // handleEdit = () => {

  // }
  render() {
    return (
      <div>
        <table className="table">
          <thead className="bg-dark text-white">
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Name</th>
              <th scope="col">Image</th>
              <th scope="col">Price</th>
              <th scope="col">Description</th>
              <th scope="col">Type</th>
              <th colSpan={2}>
                <i className="fa-solid fa-gear"></i>
              </th>
            </tr>
          </thead>
          <tbody>{this.renderListStu()}</tbody>
        </table>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteStd: (id) => {
      const action = {
        type: "DELETE_STU",
        payLoad: id,
      };
      dispatch(action);
    },
    onEditStd: (user) => {
      const action = {
        type: "EDIT_STU",
        payLoad: user,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(Student);
