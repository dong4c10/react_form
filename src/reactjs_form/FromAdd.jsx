import React, { Component } from "react";
import { connect } from "react-redux";
import Search from "./Search";
class FromAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      image: "",
      type: "",
      price: 0,
      description: "",
    };
  }
  handleOnchange = (e) => {
    const { name, value } = e.target;
    console.log(name, value);
    this.setState({
      [name]: value,
    });
  };
  handleOnsubmit = (e) => {
    //    ngăn load lại trang
    e.preventDefault();
    console.log(this.state);
    this.props.onSubmit(this.state);
    this.setState({
      id: "",
      fullName: "",
      Phone: "",
      Email: "",
    });
  };
  render() {
    return (
      <div>
        <form onSubmit={this.handleOnsubmit}>
          <div className="box">
            <div className="box_1">
              <div className="form-group">
                <label>Id</label>
                <input
                  type="text"
                  className="form-control"
                  name="id"
                  id="id"
                  onChange={this.handleOnchange}
                  value={this.state.id}
                />
              </div>
              <div className="form-group">
                <label>Name</label>
                <input
                  type="text"
                  className="form-control"
                  name="name"
                  onChange={this.handleOnchange}
                  value={this.state.name}
                />
              </div>
              <div className="form-group">
                <label>Product Type</label>
                <select
                  value={this.state.type}
                  onChange={this.handleOnchange}
                  className="form-control"
                  id="type"
                  name="type"
                  onBlur={this.handleOnBlur}
                >
                  <option>Apple</option>
                  <option>Samsung</option>
                </select>
              </div>
            </div>
            <div className="box_1">
              <div className="form-group">
                <label>Image</label>
                <input
                  type="text"
                  className="form-control"
                  name="image"
                  onChange={this.handleOnchange}
                  value={this.state.image}
                />
              </div>
              <div className="form-group">
                <label>Price</label>
                <input
                  type="text"
                  className="form-control"
                  name="price"
                  onChange={this.handleOnchange}
                  value={this.state.price}
                />
              </div>
              <div className="form-group">
                <label>Description</label>
                <input
                  type="text"
                  className="form-control"
                  name="description"
                  onChange={this.handleOnchange}
                  value={this.state.description}
                />
              </div>
            </div>
          </div>
          <div>
            <div className="btn1 py-3">
              <button type="submit" className="btn btn-success">
                Thêm Sản Phẩm
              </button>
            </div>
            <Search />
          </div>
        </form>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (student) => {
      const action = {
        type: "SUBMIT_FROM_STUDENT",
        payLoad: student,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(FromAdd);
